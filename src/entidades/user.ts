export interface User{
    id?:number;
    name:string;
    username:string;
    email:string;
    adress?: UserAdress;
    phone: string;
    website:string;
    company?: UserCompany;
}

interface UserAdress{
    street: string;
    suite: string;
    city: string;
    zipcode: string;
    geo: UserGeoLocation;
}

interface UserCompany{
    name: string;
    catchPhrase: string;
    bs:string;
}

interface UserGeoLocation{
    lat: string;
    lng: string;
}