import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { RouterModule } from '@angular/router';
import { TasksMainViewComponent } from './tasks-main-view/tasks-main-view.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { QueryTasksComponent } from './query-tasks/query-tasks.component';
import { UserTaskFilterComponent } from './user-task-filter/user-task-filter.component'
import { MatSelectModule } from '@angular/material/select';
import { RegisterTaskComponent } from './register-task/register-task.component';
import { ReactiveFormsModule } from '@angular/forms';
import { EditTaskComponent } from './edit-task/edit-task.component';

@NgModule({
  declarations: [
    AppComponent,
    TasksMainViewComponent,
    TopBarComponent,
    QueryTasksComponent,
    UserTaskFilterComponent,
    RegisterTaskComponent,
    EditTaskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatTableModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: TasksMainViewComponent, data: {title: 'Home'}},
      {path: 'querytasks', component: QueryTasksComponent, data: {title: 'Tarefas'}},
      {path: 'filter', component: UserTaskFilterComponent, data: {title: 'Tarefas por Usuário'}},
      {path:'registertasks', component: RegisterTaskComponent, data: {title: 'Cadastrar Tarefa'}},
      {path:'edittask/:id', component: EditTaskComponent, data: {title: 'Editar Tarefa'}}
    ]),
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSelectModule,
    MatTableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
