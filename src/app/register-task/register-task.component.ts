import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TaskService } from '../task.service';
import { Task } from '../../entidades/task';
import { UserService } from '../user.service';
import { User } from '../../entidades/user';

@Component({
  selector: 'app-register-task',
  templateUrl: './register-task.component.html',
  styleUrls: ['./register-task.component.css']
})
export class RegisterTaskComponent implements OnInit {

  tasks;
  users;
  selectedUser: User;
  checkoutForm: FormGroup;
  public mode: String = 'list';

  constructor(
    private taskService: TaskService,
    private userService: UserService,
    private formBuilder: FormBuilder
  ) { 
    this.checkoutForm = this.formBuilder.group({
      name: ''
    });
  }

  ngOnInit(): void {
    this.tasks = this.taskService.getTasks().subscribe(tasks => this.tasks = tasks);
    this.users = this.userService.getUsers().subscribe(users => this.users = users);
    this.selectedUser = this.users[2].name;
  }

  onSubmit (customerData) {
    this.taskService.getTasksFromUser(customerData);
    this.checkoutForm.reset();
}

  add () {
    window.alert('Tarefa adiciona com sucesso!');
  }
}
