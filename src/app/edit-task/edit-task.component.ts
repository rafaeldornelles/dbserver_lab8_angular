import { Component, OnInit, NgModule } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { TaskService } from '../task.service';
import { Task } from '../../entidades/task';
import { UserService } from '../user.service';
import { User } from '../../entidades/user';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from '../app.component';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-edit-task',
  templateUrl: './edit-task.component.html',
  styleUrls: ['./edit-task.component.css']

})


export class EditTaskComponent implements OnInit {
  asyncString;
  task: Task;
  users;
  user: User;
  selectedUser: User;
  checkoutForm: FormGroup;
  idTask: number;
  public mode: String = 'list';

  constructor(
    private taskService: TaskService,
    private userService: UserService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
  ) {
    this.checkoutForm = this.formBuilder.group({
      statusTarefa: '',
      tituloTarefa: '',
      idUsuario: ''
    });
    // para captar o id da tarefa
    this.route.params.subscribe(params => this.idTask = params['id']);


  }

  ngOnInit(): void {
    // para listar os nomes no dropdown
    this.users = this.userService.getUsers().subscribe(users => this.users = users);
    this.selectedUser = this.users.name;

  }
  // tslint:disable-next-line: typedef
  onSubmit () {
    // transformando de observable para um objeto - objeto task
    // chamando aux
    this.asyncString = this.taskService.getTaskById(this.idTask).subscribe(valForm => this.aux(valForm));
  }

  aux (valForm) {
       // pega id do usuário e chama convertTask
        // tslint:disable-next-line: max-line-length
    this.asyncString = this.userService.getUserById(this.checkoutForm.get('idUsuario').value).subscribe(valUser => this.convertTask(valForm, valUser));
   }
// tratamento de dados - chama o updateTask
  convertTask (valForm, valUser) {
    this.task = valForm;
    this.task.userId = valUser.id;
    this.task.user = valUser;
    if(this.checkoutForm.get('statusTarefa').value){
      this.task.completed = true;
    }else{
      this.task.completed = false;
    }

    this.taskService.updateTask(this.idTask, this.task);

   }

// tslint:disable-next-line: typedef
add () {
  window.alert('Tarefa atualizada com sucesso!');
}

}
