import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../environments/environment';
import { User } from '../entidades/user';
import { Observable, throwError, of } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    public http: HttpClient
  ) { }

  getUsers(): Observable<User[]>{
    return this.http.get<User[]>(environment.WSURL + 'users').pipe(
      catchError(this.errorHandler<User[]>([], 'Erro ao buscar os usuários'))
    );
  }

  getUserById(id:number): Observable<User>{
    return this.http.get<User>(environment.WSURL + 'users/' + id).pipe(
      catchError(this.errorHandler<User>(null, `Erro ao buscar o usuário ${id}`))
    );
  }

  createUser(user:User): Observable<User>{
    return this.http.post<User>(environment.WSURL + 'users', user).pipe(
      catchError(this.errorHandler<User>(null, 'Erro ao cadastrar o usuário'))
    );
  }

  deleteUser(id:number): Observable<null>{
    return this.http.delete<null>(environment.WSURL + 'users/' + id).pipe(
      catchError(this.errorHandler<null>(null, 'Erro ao deletar o usuário'))
    );
  }

  updateUser(id:number, user:User):Observable<User>{
    return this.http.put<User>(environment.WSURL + 'users/' + id, user).pipe(
      catchError(this.errorHandler<User>(null, 'Erro ao atualizar o usuário'))
    );
  }

  patchUser(id:number, user:Object): Observable<User>{
    return this.http.patch<User>(environment.WSURL + 'users/' + id, user).pipe(
      catchError(this.errorHandler<User>(null, 'Erro ao atualizar o usuário'))
    );
  }

  errorHandler<T>(resultado?: T, mensagem:string = 'Erro'){
    return (erro: HttpErrorResponse): Observable<T> => {
      if(erro.error instanceof ErrorEvent){
        console.log(`${mensagem} ${erro.error.message}`);
        return throwError(`${mensagem}: ${erro.error.message}`);
      }else{
        console.log(`${mensagem} - Status: ${erro.status}`);
        return of(resultado as T);
      }
    }
  }
}
