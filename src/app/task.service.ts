import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable, forkJoin, throwError, of } from 'rxjs';
import { Task } from '../entidades/task';
import { User } from '../entidades/user';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';
import { map, mergeMap, catchError } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(
    public http: HttpClient,
    public userService: UserService
  ) { }

  getTasks(): Observable<Task[]>{
    return this.http.get<Task[]>(environment.WSURL + 'todos').pipe(
      map(tasks => tasks.map(task => this.populateTask(task))),
      mergeMap(result => forkJoin(result)),
      catchError(this.errorHandler<Task[]>([], 'Erro ao buscar as tarefas'))
    );
  }

  getTaskById(id: number): Observable<Task>{
    return this.http.get<Task>(environment.WSURL + 'todos/' + id).pipe(
      mergeMap(task => this.populateTask(task)),
      catchError(this.errorHandler<Task>(null, `Erro ao buscar a tarefa ${id}`))
    );
  }

  getTasksFromUser(userId: number): Observable<Task[]>{
    return this.http.get<Task[]>(environment.WSURL + 'todos?userId='+ userId).pipe(
      mergeMap(tasks => this.populateTasksWithUser(tasks, userId)),
      catchError(this.errorHandler<Task[]>([], 'Erro ao buscar as tarefas'))
    );
  }

  createTask(task: Task): Observable<Task>{
    return this.http.post<Task>(environment.WSURL + 'todos', task).pipe(
      mergeMap(task => this.populateTask(task)),
      catchError(this.errorHandler<Task>(null, 'Erro ao cadastrar tarefa'))
    );
  }

  deleteTask(id:number): Observable<null>{
    return this.http.delete<null>(environment.WSURL + 'todos/' + id).pipe(
      catchError(this.errorHandler<null>(null, 'Erro ao deletar a tarefa'))
    );
  }

  updateTask(id:number, task:Task): Observable<Task>{
    console.log("id: "+ id);
    console.log(task);
    return this.http.put<Task>(environment.WSURL + 'todos/' + id, task).pipe(
      mergeMap(task => this.populateTask(task)),
      catchError(this.errorHandler<Task>(null, 'Erro ao atualizar a tarefa'))
    );
  }

  patchTask(id:number, task:Object): Observable<Task>{
    return this.http.patch<Task>(environment.WSURL + 'todos/' + id, task).pipe(
      mergeMap(task => this.populateTask(task)),
      catchError(this.errorHandler<Task>(null, 'Erro ao atualizar a tarefa'))
    );
  }

  populateTask(task:Task): Observable<Task>{ //realiza uma requisição para cada task
    return this.userService.getUserById(task.userId).pipe(
      map(user=>{
        if(user){
          task.user = user;
        }
        return task;
      })
    );
  }

  populateTasksWithUser(tasks: Task[], userId: number): Observable<Task[]>{  //realiza apenas uma requisição para listas de tasks com o mesmo usuário
    return this.userService.getUserById(userId).pipe(
      map(user => {
        tasks = tasks.map(task => {
          if(user){
            task.user = user
          }
          return task;
        });
        return tasks;
      })
    );
  }

  errorHandler<T>(resultado?: T, mensagem:string = 'Erro'){
    return (erro: HttpErrorResponse): Observable<T> => {
      if(erro.error instanceof ErrorEvent){
        console.log(`${mensagem} ${erro.error.message}`);
        return throwError(`${mensagem}: ${erro.error.message}`);
      }else{
        console.log(`${mensagem} - Status: ${erro.status}`);
        return of(resultado as T);
      }
    }
  }

}
