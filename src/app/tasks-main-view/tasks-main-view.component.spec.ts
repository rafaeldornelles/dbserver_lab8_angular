import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TasksMainViewComponent } from './tasks-main-view.component';

describe('TasksMainViewComponent', () => {
  let component: TasksMainViewComponent;
  let fixture: ComponentFixture<TasksMainViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TasksMainViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TasksMainViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
