import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task.service';
import { Task } from '../../entidades/task';

@Component({
  selector: 'app-query-tasks',
  templateUrl: './query-tasks.component.html',
  styleUrls: ['./query-tasks.component.css']
})
export class QueryTasksComponent implements OnInit {
  tasks: Task[];
  displayedColumns = ['title', 'completed', 'user'];
  constructor(private taskService: TaskService) { }

  ngOnInit(): void {
    this.taskService.getTasks().subscribe(tasks => this.tasks = tasks);
  }
}

