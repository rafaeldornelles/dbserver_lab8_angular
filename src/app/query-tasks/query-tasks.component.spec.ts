import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QueryTasksComponent } from './query-tasks.component';

describe('QueryTasksComponent', () => {
  let component: QueryTasksComponent;
  let fixture: ComponentFixture<QueryTasksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QueryTasksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QueryTasksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
