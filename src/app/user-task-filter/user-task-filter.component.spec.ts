import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserTaskFilterComponent } from './user-task-filter.component';

describe('UserTaskFilterComponent', () => {
  let component: UserTaskFilterComponent;
  let fixture: ComponentFixture<UserTaskFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserTaskFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserTaskFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
