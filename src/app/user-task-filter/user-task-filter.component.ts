import { Component, OnInit } from '@angular/core';
import { TaskService } from '../task.service'
import { UserService } from '../user.service'
import { Task } from '../../entidades/task';
import { User } from '../../entidades/user';

@Component({
  selector: 'app-user-task-filter',
  templateUrl: './user-task-filter.component.html',
  styleUrls: ['./user-task-filter.component.css']
})
export class UserTaskFilterComponent implements OnInit {

  users;
  tasks: Task[];
  selectedUser: number;

  displayedColumns: string[] = ['id', 'title', 'completed', 'userId', 'user.name'];

  constructor(
    private taskService: TaskService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.users = this.userService.getUsers().subscribe(users => this.users = users);
    this.selectedUser = this.users[2].name;

  }

  getSelectedUserTasks(event){

    this.taskService.getTasksFromUser(event.value).subscribe(tasks => this.tasks = tasks);

  }
}
